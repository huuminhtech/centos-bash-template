!/bin/bash

if [ $(id -u) != "0" ]; then
    printf "You need to be root to perform this command. Run \"sudo su\" to become root!\n"
    exit
fi
 
if [ -f /etc/huuminh.conf ]; then
	echo "Script is already installed"
    exit
fi

rm -f install*
cd /root/
rm -f install*
curl -sO ""https://bitbucket.org/huuminhtech/centos-bash-template/raw/413814a84a101847c9ca6e54cbd81c1af2f954fe/7.4/install.sh"" && chmod +x install.sh && ./install.sh