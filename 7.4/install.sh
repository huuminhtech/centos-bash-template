#!/bin/bash
printf "Enter domain: "
read SERVERNAME
if [ "$SERVERNAME" = "" ]; then
	echo "Invalid Domain"
    exit
fi

SERVER_MEMORY=$(awk '/MemTotal/ {print $2}' /proc/meminfo)

printf "Setup Timezone\n" 
rm -f /etc/localtime
ln -sf /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime
if [ -s /etc/selinux/config ]; then
    sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
    sed -i 's/SELINUX=permissive/SELINUX=disabled/g' /etc/selinux/config
fi
setenforce 0

sleep 3
yum -y remove mysql* php* httpd* sendmail* postfix* rsyslog*
yum clean all
yum -y update


printf "Installing EPEL + Remi Repo\n"
yum -y install epel-release yum-utils
rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-7.rpm
sleep 3

printf "Installing EPEL + Remi Repo\n"
rpm -Uvh http://nginx.org/packages/centos/7/noarch/RPMS/nginx-release-centos-7-0.el7.ngx.noarch.rpm
sleep 3

printf "Installing Nginx, PHP..."
yum-config-manager --enable remi-php71
yum -y install nginx php-fpm php-common php-gd php-mysqlnd php-pdo php-xml php-mbstring php-mcrypt php-curl php-opcache php-cli php-pecl-zip
systemctl enable nginx.service
systemctl enable php-fpm.service
systemctl enable fail2ban.service
systemctl enable iptables.service
systemctl enable ip6tables.service

printf "Installing Redis Cache, PHP Redis..."
yum -y --enablerepo=remi,remi-php71 install redis php-pecl-redis
sudo systemctl restart php-fpm
sudo sysctl vm.overcommit_memory=1
sudo systemctl enable redis

printf "Installing Postgresql\n"
sudo yum -y install postgresql-server postgresql-contrib
sudo postgresql-setup initdb

sed -i 's/ident/md5/g' /var/lib/pgsql/data/pg_hba.conf
sudo systemctl start postgresql
sudo systemctl enable postgresql


printf "Creating default directory\n"
mkdir -p /home/$SERVERNAME/public_html
mkdir /home/$SERVERNAME/logs
chmod 777 /home/$SERVERNAME/logs

mkdir -p /var/log/nginx
chown -R nginx:nginx /var/log/nginx
chown -R nginx:nginx /var/lib/php/session

wget -q https://bitbucket.org/huuminhtech/centos-bash-template/raw/6951fd91475dffa17c311999824b876599739c23/7.4/php-fpm.conf -O /etc/php-fpm.conf
wget -q https://bitbucket.org/huuminhtech/centos-bash-template/raw/d5acbe662b04463c29b5120478d264e5e52eb308/7.4/www.conf -O /etc/php-fpm.d/www.conf

MAX_CHILD=`echo "scale=0;$SERVER_MEMORY*0.4/30" | bc`
sed -i "s/server_name_here/$SERVERNAME/g" /etc/php-fpm.conf
sed -i "s/server_name_here/$SERVERNAME/g" /etc/php-fpm.d/www.conf
sed -i "s/max_children_here/$MAX_CHILD/g" /etc/php-fpm.d/www.conf

# dynamic PHP memory_limit calculation
if [[ "$SERVER_MEMORY" -le '262144' ]]; then
	php_memorylimit='48M'
	php_uploadlimit='48M'
	php_realpathlimit='256k'
	php_realpathttl='14400'
elif [[ "$SERVER_MEMORY" -gt '262144' && "$SERVER_MEMORY" -le '393216' ]]; then
	php_memorylimit='96M'
	php_uploadlimit='96M'
	php_realpathlimit='320k'
	php_realpathttl='21600'
elif [[ "$SERVER_MEMORY" -gt '393216' && "$SERVER_MEMORY" -le '524288' ]]; then
	php_memorylimit='128M'
	php_uploadlimit='128M'
	php_realpathlimit='384k'
	php_realpathttl='28800'
elif [[ "$SERVER_MEMORY" -gt '524288' && "$SERVER_MEMORY" -le '1049576' ]]; then
	php_memorylimit='160M'
	php_uploadlimit='160M'
	php_realpathlimit='384k'
	php_realpathttl='28800'
elif [[ "$SERVER_MEMORY" -gt '1049576' && "$SERVER_MEMORY" -le '2097152' ]]; then
	php_memorylimit='256M'
	php_uploadlimit='256M'
	php_realpathlimit='384k'
	php_realpathttl='28800'
elif [[ "$SERVER_MEMORY" -gt '2097152' && "$SERVER_MEMORY" -le '3145728' ]]; then
	php_memorylimit='320M'
	php_uploadlimit='320M'
	php_realpathlimit='512k'
	php_realpathttl='43200'
elif [[ "$SERVER_MEMORY" -gt '3145728' && "$SERVER_MEMORY" -le '4194304' ]]; then
	php_memorylimit='512M'
	php_uploadlimit='512M'
	php_realpathlimit='512k'
	php_realpathttl='43200'
elif [[ "$SERVER_MEMORY" -gt '4194304' ]]; then
	php_memorylimit='800M'
	php_uploadlimit='800M'
	php_realpathlimit='640k'
	php_realpathttl='86400'
fi

cat > "/etc/php.d/00-huuminh-custom.ini" <<END
date.timezone = Asia/Ho_Chi_Minh
max_execution_time = 180
short_open_tag = On
realpath_cache_size = $php_realpathlimit
realpath_cache_ttl = $php_realpathttl
memory_limit = $php_memorylimit
upload_max_filesize = $php_uploadlimit
post_max_size = $php_uploadlimit
expose_php = Off
mail.add_x_header = Off
max_input_nesting_level = 128
max_input_vars = 2000
mysqlnd.net_cmd_buffer_size = 16384
always_populate_raw_post_data=-1
disable_functions=shell_exec
END

print "Installing OPCache"

cat > /etc/php.d/*opcache*.ini <<END
zend_extension=opcache.so
opcache.enable=1
opcache.enable_cli=1
opcache.memory_consumption=128
opcache.interned_strings_buffer=16
opcache.max_accelerated_files=4000
opcache.max_wasted_percentage=5
opcache.use_cwd=1
opcache.validate_timestamps=1
opcache.revalidate_freq=60
opcache.fast_shutdown=1
opcache.blacklist_filename=/etc/php.d/opcache-default.blacklist
END

cat > /etc/php.d/opcache-default.blacklist <<END

END

systemctl restart php-fpm.service

sleep 3

print "Nginx config\n"

cat > "/etc/nginx/nginx.conf" <<END

user nginx;
worker_processes auto;
worker_rlimit_nofile 260000;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;

events {
	worker_connections  2048;
	accept_mutex off;
	accept_mutex_delay 200ms;
	use epoll;
	#multi_accept on;
}

http {
	include       /etc/nginx/mime.types;
	default_type  application/octet-stream;

	log_format  main  '\$remote_addr - \$remote_user [\$time_local] "\$request" '
	              '\$status \$body_bytes_sent "\$http_referer" '
	              '"\$http_user_agent" "\$http_x_forwarded_for"';

	#Disable IFRAME
	add_header X-Frame-Options SAMEORIGIN;
	
	#Prevent Cross-site scripting (XSS) attacks
	add_header X-XSS-Protection "1; mode=block";
	
	#Prevent MIME-sniffing
	add_header X-Content-Type-Options nosniff;
	
	access_log  off;
	sendfile on;
	tcp_nopush on;
	tcp_nodelay off;
	types_hash_max_size 2048;
	server_tokens off;
	server_names_hash_bucket_size 128;
	client_max_body_size 0;
	client_body_buffer_size 256k;
	client_body_in_file_only off;
	client_body_timeout 60s;
	client_header_buffer_size 256k;
	client_header_timeout  20s;
	large_client_header_buffers 8 256k;
	keepalive_timeout 10;
	keepalive_disable msie6;
	reset_timedout_connection on;
	send_timeout 60s;
	
	gzip on;
	gzip_static on;
	gzip_disable "msie6";
	gzip_vary on;
	gzip_proxied any;
	gzip_comp_level 6;
	gzip_buffers 16 8k;
	gzip_http_version 1.1;
	gzip_types text/plain text/css application/json text/javascript application/javascript text/xml application/xml application/xml+rss;

	include /etc/nginx/conf.d/*.conf;
}
END


rm -rf /etc/nginx/conf.d/*
> /etc/nginx/conf.d/default.conf

server_name_alias="www.$SERVERNAME"
if [[ $SERVERNAME == *www* ]]; then
    server_name_alias=${SERVERNAME/www./''}
fi

cat > "/etc/nginx/conf.d/$SERVERNAME.conf" <<END
server {
	listen 80;
	
	server_name $server_name_alias;
	rewrite ^(.*) http://$SERVERNAME\$1 permanent;
}

server {
	listen 80 default_server;
		
	# access_log off;
	access_log /home/$SERVERNAME/logs/access.log;
	# error_log off;
    	error_log /home/$SERVERNAME/logs/error.log;
	
    	root /home/$SERVERNAME/public_html;
	index index.php index.html index.htm;
    	server_name $SERVERNAME;
 
    	location / {
		try_files \$uri \$uri/ /index.php?\$args;
	}
	
	# Custom configuration
	include /home/$SERVERNAME/public_html/*.conf;
 
    	location ~ \.php$ {
		fastcgi_split_path_info ^(.+\.php)(/.+)$;
        	include /etc/nginx/fastcgi_params;
        	fastcgi_pass 127.0.0.1:9000;
        	fastcgi_index index.php;
		fastcgi_connect_timeout 1000;
		fastcgi_send_timeout 1000;
		fastcgi_read_timeout 1000;
		fastcgi_buffer_size 256k;
		fastcgi_buffers 4 256k;
		fastcgi_busy_buffers_size 256k;
		fastcgi_temp_file_write_size 256k;
		fastcgi_intercept_errors on;
        	fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    	}
	
	location /nginx_status {
  		stub_status on;
  		access_log   off;
		allow 127.0.0.1;
		deny all;
	}
	
	location /php_status {
		fastcgi_pass 127.0.0.1:9000;
		fastcgi_index index.php;
		fastcgi_param SCRIPT_FILENAME  \$document_root\$fastcgi_script_name;
		include /etc/nginx/fastcgi_params;
		allow 127.0.0.1;
		deny all;
    	}
	
	# Disable .htaccess and other hidden files
	location ~ /\.(?!well-known).* {
		deny all;
		access_log off;
		log_not_found off;
	}
	
        location = /favicon.ico {
                log_not_found off;
                access_log off;
        }
	
	location = /robots.txt {
		allow all;
		log_not_found off;
		access_log off;
	}
	
	location ~* \.(3gp|gif|jpg|jpeg|png|ico|wmv|avi|asf|asx|mpg|mpeg|mp4|pls|mp3|mid|wav|swf|flv|exe|zip|tar|rar|gz|tgz|bz2|uha|7z|doc|docx|xls|xlsx|pdf|iso|eot|svg|ttf|woff)$ {
	        gzip_static off;
		add_header Pragma public;
		add_header Cache-Control "public, must-revalidate, proxy-revalidate";
		access_log off;
		expires 30d;
		break;
        }

        location ~* \.(txt|js|css)$ {
	        add_header Pragma public;
		add_header Cache-Control "public, must-revalidate, proxy-revalidate";
		access_log off;
		expires 30d;
		break;
        }
}
END

cat >> "/etc/security/limits.conf" <<END
* soft nofile 262144
* hard nofile 262144
nginx soft nofile 262144
nginx hard nofile 262144
nobody soft nofile 262144
nobody hard nofile 262144
root soft nofile 262144
root hard nofile 262144
END

ulimit -n 262144
systemctl restart nginx.service
sleep 3
clear
yum erase -y sendmail*
printf "Installing postfix"

yum -y install postfix cyrus-sasl-plain mailx
systemctl restart postfix
systemctl enable postfix

clear
printf "\nSMTP Server ([smtp.mailgun.org]:587): "
read SMTP_SERVER
if [ "$SMTP_SERVER" = "" ]; then
	SMTP_SERVER="[smtp.mailgun.org]:587"
fi
printf "\nSMTP Username: "
read SMTP_USERNAME
if [ "$SMTP_USERNAME" = "" ]; then
	SMTP_USERNAME="postmaster@domain.com"
fi
printf "\nSMTP Password: "
read SMTP_PASSWORD
if [ "$SMTP_PASSWORD" = "" ]; then
	SMTP_PASSWORD="password"
fi

cp "/etc/postfix/main.cf" "/etc/postfix/main.cf.bk"
cat >> "/etc/postfix/main.cf" <<END
queue_directory = /var/spool/postfix
command_directory = /usr/sbin
daemon_directory = /usr/libexec/postfix
data_directory = /var/lib/postfix
mail_owner = postfix
#myhostname = host.domain.tld
#myhostname = virtual.domain.tld

mydomain = leech360.com

#myorigin = $myhostname
#myorigin = $mydomain
inet_interfaces = localhost

# Enable IPv4, and IPv6 if supported
inet_protocols = all
mydestination = $myhostname, localhost.$mydomain, localhost
unknown_local_recipient_reject_code = 550
smtp_sasl_auth_enable=yes
smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
smtp_sasl_security_options = noanonymous
smtp_sasl_tls_security_options = noanonymous
smtp_use_tls = yes 
relayhost = $SMTP_SERVER

alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases
debug_peer_level = 2
debugger_command =
	 PATH=/bin:/usr/bin:/usr/local/bin:/usr/X11R6/bin
	 ddd $daemon_directory/$process_name $process_id & sleep 5
sendmail_path = /usr/sbin/sendmail.postfix
newaliases_path = /usr/bin/newaliases.postfix
mailq_path = /usr/bin/mailq.postfix
setgid_group = postdrop
html_directory = no
manpage_directory = /usr/share/man
sample_directory = /usr/share/doc/postfix-2.10.1/samples
readme_directory = /usr/share/doc/postfix-2.10.1/README_FILES
END

echo "$SMTP_SERVER $SMTP_USERNAME:$SMTP_PASSWORD" > /etc/postfix/sasl_passwd
postmap /etc/postfix/sasl_passwd
chown root:postfix /etc/postfix/sasl_passwd*
chmod 640 /etc/postfix/sasl_passwd*
 
systemctl reload postfix
echo "This is a test." | mail -s "test message" "minh@huuminh.net"

printf "\nSetup IPTables, Fail2ban and SSH Port"
# Change port SSH
sed -i 's/#Port 22/Port 1991/g' /etc/ssh/sshd_config


cat > "/etc/fail2ban/jail.local" <<END
[sshd]
enabled  = true
filter   = sshd
action   = iptables[name=SSH, port=1991, protocol=tcp]
logpath  = /var/log/secure
maxretry = 3
bantime = 3600
END

# Log Rotation
cat > "/etc/logrotate.d/nginx" <<END
/home/*/logs/access.log /home/*/logs/error.log /home/*/logs/nginx_error.log {
	create 640 nginx nginx
        daily
	dateext
        missingok
        rotate 5
        maxage 7
        compress
	size=100M
        notifempty
        sharedscripts
        postrotate
                [ -f /var/run/nginx.pid ] && kill -USR1 \`cat /var/run/nginx.pid\`
        endscript
	su nginx nginx
}
END
cat > "/etc/logrotate.d/php-fpm" <<END
/home/*/logs/php-fpm*.log {
        daily
	dateext
        compress
        maxage 7
        missingok
        notifempty
        sharedscripts
        size=100M
        postrotate
            /bin/kill -SIGUSR1 \`cat /var/run/php-fpm/php-fpm.pid 2>/dev/null\` 2>/dev/null || true
        endscript
	su nginx nginx
}
END

systemctl start fail2ban.service
systemctl restart fail2ban.service

# Open port
if [ -f /etc/sysconfig/iptables ]; then
	iptables -I INPUT -p tcp --dport 80 -j ACCEPT
	iptables -I INPUT -p tcp --dport 25 -j ACCEPT
	iptables -I INPUT -p tcp --dport 443 -j ACCEPT
	iptables -I INPUT -p tcp --dport 465 -j ACCEPT
	iptables -I INPUT -p tcp --dport 587 -j ACCEPT
	iptables -I INPUT -p tcp --dport 1191 -j ACCEPT
	iptables -I INPUT -p tcp --dport 1991 -j ACCEPT
	service iptables save
fi
 
chown nginx:nginx /home/$SERVERNAME
chown -R nginx:nginx /home/*/public_html
rm -f /root/install
echo -n "cd /home/$SERVERNAME/public_html" >> /root/.bashrc